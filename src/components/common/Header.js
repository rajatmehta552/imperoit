import React from 'react';
import {TouchableItem} from '../../elements';
import {Image, StyleSheet, View, Text, FlatList} from 'react-native';
import {COLOR} from '../../constants/color';
import {connect} from 'react-redux';
import {ACTIVE_CATEGORY, SUB_CATEGORY_LOADING} from '../../reducers/types';
import {getSubCategories} from '../../actions';
import {navigateTo} from '../../helpers';
import {Routes} from '../../navigation/routes';

const Item = ({item, active, setActive}) => (
  <TouchableItem onPress={() => setActive(item.Id)} style={styles.item}>
    <Text style={[active ? styles.active : styles.title]}>{item.Name}</Text>
  </TouchableItem>
);

function Header({navigation, category, active, dispatch}) {
  const renderItem = ({item}) => (
    <Item item={item} active={item.Id === active} setActive={setActive} />
  );

  const setActive = (id) => {
    dispatch({type: ACTIVE_CATEGORY, payload: id});
    dispatch({type: SUB_CATEGORY_LOADING});
    dispatch(getSubCategories(id));
  };

  return (
    <View style={styles.wrapperMain}>
      <TouchableItem
        style={styles.filterWrapper}
        onPress={() => navigateTo(navigation, Routes.TEST_STRIP)}>
        <Image
          source={require('../../assets/images/filter.png')}
          style={styles.icon}
        />
      </TouchableItem>
      <Image
        source={require('../../assets/images/logo.png')}
        style={styles.logo}
      />
      <TouchableItem
        style={styles.searchWrapper}
        onPress={() => console.log('filter')}>
        <Image
          source={require('../../assets/images/search.png')}
          style={styles.icon}
        />
      </TouchableItem>
      <FlatList
        data={category}
        renderItem={renderItem}
        keyExtractor={(item) => item.Id}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.listWrapper}
      />
    </View>
  );
}

function mapStateToProps(state) {
  return {
    category: state.category.data,
    active: state.category.active,
  };
}

export default connect(mapStateToProps)(Header);

const styles = StyleSheet.create({
  wrapperMain: {
    backgroundColor: COLOR.black,
    height: 80,
    width: '100%',
  },
  logo: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  filterWrapper: {
    position: 'absolute',
    zIndex: 999,
    right: 60,
    top: 15,
  },
  searchWrapper: {
    position: 'absolute',
    zIndex: 999,
    right: 20,
    top: 15,
  },
  icon: {
    height: 25,
    width: 20,
    resizeMode: 'contain',
  },
  listWrapper: {
    paddingBottom: 10,
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    color: COLOR.grey,
  },
  active: {
    fontSize: 18,
    fontWeight: 'bold',
    color: COLOR.white,
  },
});
