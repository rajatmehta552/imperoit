import {
  CATEGORY_LIST,
  ACTIVE_CATEGORY,
  SUB_CATEGORY_LOADING,
  SUB_CATEGORY_LIST,
  PRODUCT_LIST,
} from './types';

const initialState = {
  data: [],
  subcategory: [],
  loading: false,
  active: null,
  page: 1,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case CATEGORY_LIST:
      return {...state, data: action.payload, loading: false};
    case ACTIVE_CATEGORY:
      return {...state, active: action.payload, loading: false, page: 1};
    case SUB_CATEGORY_LOADING:
      return {...state, loading: true, subcategory: []};
    case SUB_CATEGORY_LIST:
      const oldData = state.subcategory.filter(
        (subcategory) =>
          !action.payload.data.some((data) => data.Id === subcategory.Id),
      );
      return action.payload.page === 1
        ? {...state, subcategory: action.payload.data, loading: false, page: 1}
        : {
            ...state,
            subcategory: oldData.concat(action.payload.data),
            loading: false,
            page: action.payload.page,
          };
    case PRODUCT_LIST:
      const index = state.subcategory.findIndex(
        (item) => item.Id === action.payload.id,
      );
      const oldProductData =
        index > -1
          ? state.subcategory[index].Product.filter(
              (item) =>
                !action.payload.data.some((data) => data.Id === item.Id),
            )
          : [];
      return index > -1
        ? {
            ...state,
            subcategory: [
              ...state.subcategory.slice(0, index),
              {
                ...state.subcategory[index],
                Product: oldProductData.concat(action.payload.data),
              },
              ...state.subcategory.slice(index + 1),
            ],
          }
        : state;
    default:
      return state;
  }
}
