export const COLOR = {
  white: '#ffffff',
  black: '#000000',
  red: '#b54646',
  grey: '#999fa3',
  green: '#56c12a',
  blue: '#334FFF',
};
