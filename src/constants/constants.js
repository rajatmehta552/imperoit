import {Dimensions} from 'react-native';

export const BASE_URL = 'http://esptiles.imperoserver.in/api/API/';

export const DIMINSIONS = {
  WINDOW_HEIGHT: Dimensions.get('window').height,
  WINDOW_WIDTH: Dimensions.get('window').width,
};
