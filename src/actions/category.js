import {BASE_URL} from '../constants/constants';
import {
  ACTIVE_CATEGORY,
  CATEGORY_LIST,
  PRODUCT_LIST,
  SUB_CATEGORY_LIST,
} from '../reducers/types';

export function getCategories() {
  return (dispatch) =>
    fetch(`${BASE_URL}Product/Dashboard`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CategoryId: 0,
        DeviceManufacturer: 'Google',
        DeviceModel: 'Android SDK built for x86',
        DeviceToken: '',
        PageIndex: 1,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        if (res?.Status === 200) {
          dispatch({type: CATEGORY_LIST, payload: res.Result.Category});
          if (
            res.Result.Category?.length > 0 &&
            res.Result.Category[0].SubCategories
          ) {
            dispatch({
              type: ACTIVE_CATEGORY,
              payload: res.Result.Category[0].Id,
            });
            dispatch({
              type: SUB_CATEGORY_LIST,
              payload: {page: 1, data: res.Result.Category[0].SubCategories},
            });
          }
        } else {
          dispatch({type: CATEGORY_LIST, payload: []});
        }
      });
}

export function getSubCategories(id, page = 1) {
  return (dispatch) =>
    fetch(`${BASE_URL}Product/Dashboard`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CategoryId: id,
        PageIndex: page,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        if (
          res?.Status === 200 &&
          res.Result.Category?.length > 0 &&
          res.Result.Category[0].SubCategories
        ) {
          dispatch({
            type: SUB_CATEGORY_LIST,
            payload: {page, data: res.Result.Category[0].SubCategories},
          });
        } else {
          dispatch({
            type: SUB_CATEGORY_LIST,
            payload: {page: page - 1, data: []},
          });
        }
      });
}

export function getProducts(id, page = 1) {
  return (dispatch) =>
    fetch(`${BASE_URL}Product/ProductList`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        SubCategoryId: id,
        PageIndex: page,
      }),
    })
      .then((response) => response.json())
      .then((res) => {
        if (res?.Status === 200) {
          dispatch({
            type: PRODUCT_LIST,
            payload: {id, data: res.Result},
          });
          console.log('res', res);
        }
      });
}
