import React from 'react';
import {StatusBar, StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {COLOR} from '../constants/color';

export function ScreenContainer(props) {
  const hidden = props.hidden || false;
  const barStyle = props.barStyle || 'light-content';
  const style = props.style || {};
  return (
    <SafeAreaView
      style={[styles.container, style]}
      edges={['right', 'top', 'left']}>
      <StatusBar
        translucent
        barStyle={barStyle}
        backgroundColor={props.bg}
        hidden={hidden}
      />
      {props.children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.black,
  },
});
