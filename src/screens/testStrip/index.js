import React, {useState} from 'react';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import {COLOR} from '../../constants/color';
import {ScreenContainer, TouchableItem} from '../../elements';

const hardnessList = [
  {color: '#0000A0', code: 0},
  {color: '#4863A0', code: 110},
  {color: '#2B3856', code: 250},
  {color: '#8A4117', code: 500},
  {color: '#800080', code: 1000},
];

const chlorineList = [
  {color: '#FFFF00', code: 0},
  {color: '#BCE954', code: 1},
  {color: '#CCFB5D', code: 3},
  {color: '#9DC209', code: 5},
  {color: '#64E986', code: 10},
];

const freeChlorineList = [
  {color: '#FFFF00', code: 0},
  {color: '#848b79', code: 1},
  {color: '#2B3856', code: 3},
  {color: '#8A4117', code: 5},
  {color: '#800080', code: 10},
];

const phList = [
  {color: '#C19A6B', code: 6.2},
  {color: '#B87333', code: 6.8},
  {color: '#C47451', code: 7.2},
  {color: '#C35817', code: 7.8},
  {color: '#C85A17', code: 8.4},
];

const alkalinityList = [
  {color: '#C19A6B', code: 0},
  {color: '#8BB381', code: 40},
  {color: '#89C35C', code: 120},
  {color: '#4CC552', code: 180},
  {color: '#3EA055', code: 240},
];

const cyanuricAcidList = [
  {color: '#C19A6B', code: 0},
  {color: '#C35817', code: 50},
  {color: '#BCE954', code: 100},
  {color: '#B87333', code: 150},
  {color: '#800080', code: 300},
];

const ActiveColor = ({color, margin = 80}) => (
  <View
    style={{
      backgroundColor: color,
      height: 20,
      marginTop: margin,
    }}
  />
);

const Heading = ({title, value}) => (
  <View style={styles.rowView}>
    <Text style={styles.text}>{title}</Text>
    <View style={styles.input}>
      <Text style={styles.text}>{value}</Text>
    </View>
  </View>
);

const ColorList = ({list, onPress}) => (
  <View style={[styles.rowView]}>
    {list.map((item) => (
      <View key={item.code} style={styles.center}>
        <TouchableItem
          style={[
            styles.item,
            {
              backgroundColor: item.color,
            },
          ]}
          onPress={() => onPress(item.code)}
        />
        <Text style={styles.code}>{item.code}</Text>
      </View>
    ))}
  </View>
);

function TestStripScreen({navigation}) {
  const [hardness, setHardness] = useState(0);
  const [chlorine, setChlorine] = useState(0);
  const [freeChlorine, setFreeChlorine] = useState(0);
  const [ph, setPh] = useState(6.2);
  const [alkalinity, setAlkalinity] = useState(40);
  const [cyanuricAcid, setCyanuricAcid] = useState(50);

  const hardnessIndex = hardnessList.findIndex(
    (item) => item.code === hardness,
  );
  const chlorineIndex = chlorineList.findIndex(
    (item) => item.code === chlorine,
  );
  const freeChlorineIndex = freeChlorineList.findIndex(
    (item) => item.code === freeChlorine,
  );
  const phIndex = phList.findIndex((item) => item.code === ph);
  const alkalinityIndex = alkalinityList.findIndex(
    (item) => item.code === alkalinity,
  );
  const cyanuricIndex = cyanuricAcidList.findIndex(
    (item) => item.code === cyanuricAcid,
  );

  return (
    <ScreenContainer hidden={true} style={{backgroundColor: COLOR.white}}>
      <View style={styles.flexWrapperMain}>
        <Text style={styles.title}>Test Strip</Text>
        <ScrollView>
          <View style={styles.mainWrapper}>
            <View style={styles.leftWrapper}>
              <ActiveColor
                color={hardnessList[hardnessIndex].color}
                margin={48}
              />
              <ActiveColor color={chlorineList[chlorineIndex].color} />
              <ActiveColor color={freeChlorineList[freeChlorineIndex].color} />
              <ActiveColor color={phList[phIndex].color} />
              <ActiveColor color={alkalinityList[alkalinityIndex].color} />
              <ActiveColor color={cyanuricAcidList[cyanuricIndex].color} />
            </View>
            <View style={{width: '90%'}}>
              <Heading title={'Total Hardness (ppm)'} value={hardness} />
              <ColorList list={hardnessList} onPress={setHardness} />
              <Heading title={'Total Chlorine (ppm)'} value={chlorine} />
              <ColorList list={chlorineList} onPress={setChlorine} />
              <Heading title={'Free Chlorine (ppm)'} value={freeChlorine} />
              <ColorList list={freeChlorineList} onPress={setFreeChlorine} />
              <Heading title={'pH (ppm)'} value={ph} />
              <ColorList list={phList} onPress={setPh} />
              <Heading title={'Total Alkalinity (ppm)'} value={alkalinity} />
              <ColorList list={alkalinityList} onPress={setAlkalinity} />
              <Heading title={'Cyanuric Acid (ppm)'} value={cyanuricAcid} />
              <ColorList list={cyanuricAcidList} onPress={setCyanuricAcid} />
            </View>
          </View>
        </ScrollView>
      </View>
    </ScreenContainer>
  );
}

const styles = StyleSheet.create({
  flexWrapperMain: {
    flex: 1,
    backgroundColor: COLOR.white,
    padding: 20,
  },
  mainWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 25,
  },
  rowView: {
    marginTop: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 32,
    color: '#0000A0',
    fontWeight: 'bold',
    marginTop: 25,
  },
  leftWrapper: {
    width: '7%',
    borderWidth: 0.5,
    borderColor: COLOR.grey,
    marginTop: 10,
    borderRadius: 3,
  },
  input: {
    width: 70,
    height: 30,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: COLOR.grey,
  },
  item: {
    width: 60,
    height: 20,
    borderRadius: 5,
  },
  center: {justifyContent: 'center', alignItems: 'center'},
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: COLOR.grey,
  },
  code: {fontSize: 12, fontWeight: 'bold', color: COLOR.grey, marginTop: 5},
});

export default TestStripScreen;
