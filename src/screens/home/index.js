import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  FlatList,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import {ScreenContainer} from '../../elements';
import {COLOR} from '../../constants/color';
import Header from '../../components/common/Header';
import {DIMINSIONS} from '../../constants/constants';
import {getCategories, getProducts, getSubCategories} from '../../actions';
import {SUB_CATEGORY_LOADING} from '../../reducers/types';

const Item = ({item}) => (
  <View style={styles.item}>
    <Image source={{uri: item.ImageName}} style={styles.media} />
    <View style={styles.boxWrapper}>
      <Text style={styles.boxText}>{item.PriceCode}</Text>
    </View>
    <Text style={styles.title}>{item.Name}</Text>
  </View>
);

const renderItem = ({item}) => <Item item={item} />;

function HomeScreen({
  navigation,
  dispatch,
  subcategories,
  loading,
  active,
  page,
}) {
  const [refreshing, setRefreshing] = useState(false);
  const [product_pages, setProductPage] = useState([]);
  useEffect(() => {
    (async function f() {
      await onRefresh();
    })();
  }, []);

  const showLoader = () => (
    <View style={styles.loader}>
      <ActivityIndicator size={'large'} color={COLOR.black} />
    </View>
  );

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    setProductPage([]);
    await dispatch({type: SUB_CATEGORY_LOADING});
    await dispatch(getCategories());
    setRefreshing(false);
  }, [dispatch]);

  const loadMoreData = async () => {
    dispatch(getSubCategories(active, page + 1));
  };

  const loadMore = async (id) => {
    if (id && typeof id !== 'undefined') {
      let pageNumber = 1;
      const index = product_pages.findIndex(
        (product_page) => product_page.id === id,
      );
      let newArr = [...product_pages];
      if (index > -1) {
        newArr[index].page = newArr[index].page + 1;
        pageNumber = newArr[index].page + 1;
        setProductPage(newArr);
      } else {
        setProductPage([...product_pages, {id, page: pageNumber}]);
      }
      await dispatch(getProducts(id, pageNumber));
    }
  };

  return (
    <ScreenContainer>
      <Header navigation={navigation} />
      {loading ? (
        showLoader()
      ) : (
        <View style={styles.flexWrapperMain}>
          <ScrollView
            onScrollEndDrag={loadMoreData}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            showsVerticalScrollIndicator={false}>
            {subcategories.map((subCategory) => (
              <View key={`${subCategory.Id}`}>
                <Text style={styles.subcategory}>{subCategory.Name}</Text>
                {subCategory.Product?.length > 0 && (
                  <FlatList
                    data={subCategory.Product}
                    renderItem={renderItem}
                    keyExtractor={(item) => `${item.Id}`}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    onEndReachedThreshold={0.5}
                    onEndReached={() => loadMore(subCategory.Id)}
                    onRefresh={onRefresh}
                    refreshing={refreshing}
                  />
                )}
              </View>
            ))}
          </ScrollView>
        </View>
      )}
    </ScreenContainer>
  );
}

const styles = StyleSheet.create({
  flexWrapperMain: {
    flex: 1,
    backgroundColor: COLOR.white,
    paddingBottom: 20,
  },
  subcategory: {
    fontSize: 18,
    color: COLOR.black,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 10,
  },
  item: {
    justifyContent: 'space-between',
    marginLeft: 20,
    flex: 1,
    width: DIMINSIONS.WINDOW_WIDTH / 4,
  },
  media: {
    height: 100,
    width: DIMINSIONS.WINDOW_WIDTH / 4,
    borderRadius: 10,
    backgroundColor: COLOR.grey,
  },
  title: {
    fontSize: 12,
    marginTop: 10,
    marginLeft: 5,
  },
  boxWrapper: {
    backgroundColor: COLOR.blue,
    height: 25,
    width: 60,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 10,
    top: 10,
  },
  boxText: {
    fontSize: 14,
    color: COLOR.white,
    fontWeight: 'bold',
  },
  loader: {
    flex: 1,
    backgroundColor: COLOR.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return {
    categories: state.category.data,
    active: state.category.active,
    page: state.category.page,
    loading: state.category.loading,
    subcategories: state.category.subcategory,
  };
}

export default connect(mapStateToProps)(HomeScreen);
